#pragma once

#include <algorithm>
#include <cmath>

// Position of vertex
struct Position {
    double x{};
    double y{};
    double z{};

    Position() = default;

    Position(double x, double y, double z)
        : x(x)
        , y(y)
        , z(z) {
    }

    struct less {
        [[nodiscard]] bool operator()(const Position& lhs, const Position& rhs) const {
            return lhs.x < rhs.x || (lhs.x == rhs.x && lhs.y < rhs.y) || (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z < rhs.z);
        }
    };

    void MinForEachCoordinate(const Position& pos) {
        x = std::min(x, pos.x);
        y = std::min(y, pos.y);
        z = std::min(z, pos.z);
    }

    void Add(const Position& pos) {
        x += pos.x;
        y += pos.y;
        z += pos.z;
    }

    [[nodiscard]] double CalcEuclDist(const Position& other) const {
        const auto diff_x = (x - other.x) * (x - other.x);
        const auto diff_y = (y - other.y) * (y - other.y);
        const auto diff_z = (z - other.z) * (z - other.z);

        return sqrt(diff_x + diff_y + diff_z);
    }
};
